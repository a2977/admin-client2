import React from 'react';
import AppBar from '@material-ui/core/AppBar';
import Button from '@material-ui/core/Button';
import HomeIcon from '@material-ui/icons/Home';
import Grid from '@material-ui/core/Grid';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import Link from '@material-ui/core/Link';
import {useHistory} from 'react-router-dom';

function Copyright() {
    return (
        <Typography variant="body2" color="textSecondary" align="center">
            {'Copyright © '}
            <Link color="inherit" href="https://material-ui.com/">
                Astronauts.se
            </Link>{' '}
            {new Date().getFullYear()}
            {'.'}
        </Typography>
    );
}

const useStyles = makeStyles((theme) => ({
    icon: {
        marginRight: theme.spacing(2),
    },
    headerContent: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(8, 0, 6),
    },
    materialButtons: {
        marginTop: theme.spacing(4),
    },
    footer: {
        backgroundColor: theme.palette.background.paper,
        padding: theme.spacing(6),
    },
    dividerSpace: {
        marginTop: theme.spacing(30),
    },
}));

export default function Album() {
    const classes = useStyles();
    const history = useHistory();
    const studentLoginHandler = () => history.push('/admin');

    return (
        <React.Fragment>
            <AppBar position="relative">
                <Toolbar>
                    <HomeIcon className={classes.icon}/>
                    <Typography variant="h5" color="inherit" align="center" style={{width: "100%"}}>
                    </Typography>
                </Toolbar>
            </AppBar>

            <main>

                <div className={classes.headerContent}>
                    <Container maxWidth="sm">
                        <Typography variant="h5" align="center" color="textSecondary" paragraph>
                            Detta är en Admin-sida
                        </Typography>
                        <div className={classes.materialButtons}>
                            <Grid container spacing={2}
                                  direction="column"
                                  justifyContent="space-evenly"
                                  alignItems="stretch">
                                <Grid item>
                                    <Button onClick={studentLoginHandler} variant="contained" color="primary">
                                        Hantera LIA-plattformen
                                    </Button>
                                </Grid>
                            </Grid>
                        </div>
                    </Container>
                </div>
            </main>
        </React.Fragment>
    );
}
