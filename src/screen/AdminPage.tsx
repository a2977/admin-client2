import React from 'react'
import { Admin, Resource } from 'react-admin'
import jsonServerProvider from 'ra-data-json-server';
import StudentList from '../components/StudentList'
import StudentCreate from '../components/StudentCreate'
import StudentEdit from '../components/StudentEdit'
import SchoolList from "../components/SchoolList";
import SchoolEdit from "../components/SchoolEdit";
import CompanyList from "../components/CompanyList";
import CompanyCreate from "../components/CompanyCreate";
import CompanyEdit from "../components/CompanyEdit";
import SchoolCreate from "../components/SchoolCreate";

function AdminPage() {
    return (
        <Admin dataProvider={jsonServerProvider('http://localhost:8080')}>
            <Resource
                name='admin/students'
                list={StudentList}
                create={StudentCreate}
                edit={StudentEdit}
            />
            <Resource
                name='admin/schools'
                list={SchoolList}
                create={SchoolCreate}
                edit={SchoolEdit}
            />
            <Resource
                name='admin/companies'
                list={CompanyList}
                create={CompanyCreate}
                edit={CompanyEdit}
            />
        </Admin>
    )
}

export default AdminPage;
