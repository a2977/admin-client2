import React from 'react';
import {BrowserRouter as Router, Switch, Route} from "react-router-dom";

import "./App.css";
import Home from "./screen/Home"
import Admin from "./screen/AdminPage"


function App() {

  return (
      <div className="App">
        <Router>
          {/* <Header /> */}
          <Switch>
            <Route exact path="/">
              <Home/>
            </Route>
              <Route exact path="/admin">
              <Admin/>
            </Route>
          </Switch>
        </Router>
      </div>
  );
}

export default App;

