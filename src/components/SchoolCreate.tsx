import React from 'react'
import { Create, SimpleForm, TextInput} from 'react-admin'

const SchoolCreate = ({...props}) => {
    return (
        <Create title='Skapa en skola' {...props}>
            <SimpleForm>
                <TextInput source='name' />
                <TextInput source='email' />
                <TextInput source='city' />
            </SimpleForm>
        </Create>
    )
}

export default SchoolCreate;
