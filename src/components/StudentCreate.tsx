import React from 'react'
import { Create, SimpleForm, TextInput } from 'react-admin'

const StudentCreate = ({...props}) => {
    return (
        <Create title='Skapa en Student' {...props}>
            <SimpleForm>
                <TextInput source='name' />
                <TextInput source='email' />
                <TextInput source='course' />
                <TextInput source='city' />
                <TextInput source='school' />
            </SimpleForm>
        </Create>
    )
}

export default StudentCreate;
