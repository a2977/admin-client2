import React from 'react'
import {
    List,
    Datagrid,
    TextField,
    EmailField,
    EditButton,
    DeleteButton,
} from 'react-admin'

const SchoolList = ({...props}) => {
    return (
        <List {...props}>
            <Datagrid>
                <TextField source='id' />
                <TextField source='name' />
                <EmailField source='email' />
                <TextField source='city' />
                <EditButton basePath='/admin/schools' />
                <DeleteButton basePath='/admin/schools' />
            </Datagrid>
        </List>
    )
}

export default SchoolList;
