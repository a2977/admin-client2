import React from 'react'
import {
    List,
    Datagrid,
    TextField,
    EmailField,
    EditButton,
    DeleteButton,
} from 'react-admin'

const StudentList = ({...props}) => {
    return (
        <List {...props}>
            <Datagrid>
                <TextField source='id' />
                <TextField source='name' />
                <EmailField source='email' />
                <TextField source='course' />
                <TextField source='city' />
                <TextField source='school' />
                <TextField source='imageId' />
                <TextField source='Cv' />
                <EditButton basePath='/admin/students' />
                <DeleteButton basePath='/admin/students' />
            </Datagrid>
        </List>
    )
}

export default StudentList;
