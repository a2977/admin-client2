import React from 'react'
import { Edit, SimpleForm, TextInput } from 'react-admin';

const SchoolEdit = ({...props}) => {
        return (
            <Edit title='Redigera Skola' {...props}>
                <SimpleForm>
                    <TextInput disabled source='id'/>
                    <TextInput source='name' />
                    <TextInput source='email' />
                    <TextInput source='city' />
                </SimpleForm>
            </Edit>
        )
}



export default SchoolEdit;
