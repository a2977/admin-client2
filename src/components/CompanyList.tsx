import React from 'react'
import {
    List,
    Datagrid,
    TextField,
    EmailField,
    EditButton,
    DeleteButton,
} from 'react-admin'

const CompanyList = ({...props}) => {
    return (
        <List {...props}>
            <Datagrid>
                <TextField source='id' />
                <TextField source='name' />
                <EmailField source='email' />
                <TextField source='profession' />
                <TextField source='city' />
                <EditButton basePath='/admin/companies' />  {/*går att ändra språk på funktionsknappar label={'redigera'}*/}
                <DeleteButton basePath='/admin/companies' />
            </Datagrid>
        </List>
    )
}

export default CompanyList;
