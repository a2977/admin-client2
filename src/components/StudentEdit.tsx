import React from 'react'
import { Edit, SimpleForm, TextInput } from 'react-admin'

const StudentEdit = ({...props}) => {
    return (
        <Edit title='Redigera Student' {...props}>
            <SimpleForm>
                <TextInput disabled source='id' />
                <TextInput source='name' />
                <TextInput source='email' />
                <TextInput source='course' />
                <TextInput source='city' />
                <TextInput source='school' />
            </SimpleForm>
        </Edit>
    )
}

export default StudentEdit;
