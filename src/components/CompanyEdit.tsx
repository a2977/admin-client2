import React from 'react'
import { Edit, SimpleForm, TextInput } from 'react-admin';

const CompanyEdit = ({...props}) => {
        return (
            <Edit title='Redigera Företag' {...props}>
                <SimpleForm>
                    <TextInput disabled source='id'/>
                    <TextInput source='name' />
                    <TextInput source='email' />
                    <TextInput source='profession' />
                    <TextInput source='city' />
                </SimpleForm>
            </Edit>
        )
}



export default CompanyEdit;
