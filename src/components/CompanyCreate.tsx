import React from 'react'
import { Create, SimpleForm, TextInput } from 'react-admin'

const CompanyCreate = ({...props}) => {
    return (
        <Create title='Skapa ett företag' {...props}>
            <SimpleForm>
                <TextInput source='name' />
                <TextInput source='email' />
                <TextInput source='profession' />
                <TextInput source='city' />
            </SimpleForm>
        </Create>
    )
}

export default CompanyCreate;
